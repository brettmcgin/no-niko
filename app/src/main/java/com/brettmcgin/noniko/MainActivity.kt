package com.brettmcgin.noniko

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import java.lang.Error

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}